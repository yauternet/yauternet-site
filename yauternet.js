function toggleMenu () {  
    const navbar = document.querySelector('.navbar')
    const burger = document.querySelector('.burger')
    
    burger.addEventListener('click', e => {    
	navbar.classList.toggle('show-nav')
    })

    document
	.querySelectorAll('.navbar li')
	.forEach(li => {
	    li.addEventListener('click', e => {
		navbar.classList.remove('show-nav')
	    })
	})
}

document.addEventListener('DOMContentLoaded', toggleMenu)
